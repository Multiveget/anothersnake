// Fill out your copyright notice in the Description page of Project Settings.


#include "IndestructibleObject.h"
#include "SnakeBase.h"
#include "Projectile.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
AIndestructibleObject::AIndestructibleObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	health = 1;
}

// Called when the game starts or when spawned
void AIndestructibleObject::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AIndestructibleObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AIndestructibleObject::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		Snake->Destroy();
	}
}

void AIndestructibleObject::InteractProjectiles(AProjectile* OverlappedProjectile)
{
	OverlappedProjectile->Destroy();
	bool bIndestructElementsIsOK = false;
	health -= 1;
	if (health == 0)
	{
		OverlappedProjectile->ParentSnake->IndestructibleElements.Remove(this);
		this->Destroy();	
	}
	int AmmountOfElements = OverlappedProjectile->ParentSnake->IndestructibleElements.Num();
	if (AmmountOfElements == 0)
		bIndestructElementsIsOK=OverlappedProjectile->ParentSnake->bNoIndestructElements = true;
	bool bSnakeElements = OverlappedProjectile->ParentSnake->bEnoughElements;
	if (bSnakeElements == true && bIndestructElementsIsOK==true)
		UKismetSystemLibrary::QuitGame(this, nullptr, EQuitPreference::Quit, false);
}

