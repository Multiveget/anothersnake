// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class AFood;
class AIndestructibleObject;
class ABonusSpeed;
class AProjectileBonus;
class AProjectile;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;
	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;
	UPROPERTY(EditDefaultsOnly)
		float ElementSize;
	UPROPERTY()
		EMovementDirection LastMoveDirecton;
	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AIndestructibleObject> IndestructibleClass;
	UPROPERTY()
		TArray<AIndestructibleObject*> IndestructibleElements;
	UPROPERTY()
		TArray<FVector> IndestrucLocations;
	UPROPERTY()
		FVector LastElemPrevLocation;
	UPROPERTY()
		FTimerHandle SpeedTimerHandle;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonusSpeed> SpeedBonusClass;
	UPROPERTY()
		FTimerHandle SpeedBonusSpawnHandler;
	UPROPERTY()
		FTimerHandle ProjectileBonusSpawnHandler;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AProjectileBonus> ProjectileBonusClass;
	UPROPERTY(BlueprintReadOnly)
		float AmmountOfProjectiles;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AProjectile> ProjectileClass;
	UPROPERTY()
		bool bEnoughElements;
	UPROPERTY()
		bool bNoIndestructElements;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void AddSnakeElement(int ElementsNum=1);
	void Move();

	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	UFUNCTION()
		void SpawnFood();
	UFUNCTION()
		void AddIndestructElement(int IndestructElements);
	UFUNCTION()
		void OnBonusSpeedOverlap();
	UFUNCTION()
		void SpawnSpeedBonus();
	UFUNCTION()
		void SpawnProjectileBonus();
	UFUNCTION()
		void Shoot();
};
