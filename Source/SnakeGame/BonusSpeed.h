// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LandBonus.h"
#include "Interactable.h"
#include "BonusSpeed.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ABonusSpeed : public ALandBonus, public IInteractable
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintNativeEvent)
		void ToggleBonusVisibility() override;
		void ToggleBonusVisibility_Implementation();
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
