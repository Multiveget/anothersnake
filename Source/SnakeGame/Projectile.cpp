// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "SnakeBase.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "IndestructibleObject.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Speed = 0.2f;
	SetActorTickInterval(Speed);
	ProjectileComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileComponent"));
	ProjectileComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ProjectileComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	ProjectileComponent->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::HandleBeginProjectliveOverlap);
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AddActorWorldOffset(Direction);
	FVector CurrentLocation = GetActorLocation();
	if (FMath::Abs(CurrentLocation.X) > 500 || FMath::Abs(CurrentLocation.Y) > 500)
	{
		this->Destroy();
	}
}

void AProjectile::SetDirection()
{
	switch (ParentSnake->LastMoveDirecton)
	{
	case(EMovementDirection::UP):
		Direction = { 50, 0, 0 };
		break;
	case(EMovementDirection::DOWN):
		Direction = { -50,0,0 };
		break;
	case(EMovementDirection::LEFT):
		Direction = { 0,50,0 };
		break;
	case(EMovementDirection::RIGHT):
		Direction = { 0,-50,0 };
		break;
	}
}

void AProjectile::HandleBeginProjectliveOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	auto Indestruct = Cast<AIndestructibleObject>(OtherActor);
	if (IsValid(Indestruct))
	{
		Indestruct->InteractProjectiles(this);
	}
}


